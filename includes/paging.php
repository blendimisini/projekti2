<?php

    require 'conn.inc.php';

    $record_per_page = 4;
    $page = '';
    $output = '';

    if (isset($_POST['page'])) {
        $page = $_POST['page'];
    }else {
        $page = 1;
    }

    $start_from = ($page - 1) * $record_per_page;

    $sql = "SELECT SUBSTRING(title, 1, 20) AS title, SUBSTRING(description_service, 1, 200) AS descript, date_added FROM services ORDER BY id DESC LIMIT $start_from, $record_per_page";
    $query = $pdo->query($sql);
    $query->execute();
    $results = $query->fetchAll();
    
    foreach ($results as $result) {
        $mounth = substr($result['date_added'], 5, 2);
        switch ($mounth) {
            case '01': $mounth = 'January';
                break;
            case '02': $mounth = 'February';
                break;
            case '03': $mounth = 'March';
                break;
            case '04': $mounth = 'April';
                break;
            case '05': $mounth = 'May';
                break;
            case '06': $mounth = 'June';
                break;
            case '07': $mounth = 'July';
                break;
            case '08': $mounth = 'August';
                break;
            case '09': $mounth = 'September';
                break;
            case '10': $mounth = 'October';
                break;
            case '11': $mounth = 'November';
                break;
            case '12': $mounth = 'December';
                break;
            default: $mounth = 'None';
                break;
        }
        
        $output .=  '<div class="box">' .
                    '<h3>'. $result['title'] .'</h3>' .
                    '<p>' . $result['descript'] . '</p>' .
                    '<p class="date">' . $mounth . ' ' . substr($result['date_added'], 8, 2) . ', '.  substr($result['date_added'], 0, 4) . '</p>' . 
                    '</div>';
    }

    $page_query = "SELECT * FROM services";
    $page_result = $pdo->query($page_query);
    $page_result->execute();
    $total_records = $page_result->rowCount();
    $total_pages = ceil($total_records / $record_per_page);

    for ($i = 1; $i <= $total_pages; $i++) { 
        $output .= '<span class="pagination-link" id="' . $i . '">' . $i . '</span>'; 
    }

    echo $output;