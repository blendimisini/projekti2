$(document).ready(function() {

    load_data();

    function load_data(page) {

        $.ajax({
            url: "includes/paging.php",
            method: "POST",
            data: {page : page},
            success: function(data) {
                $('.box-con').html(data);
            } 
        })
    }

    $(document).on('click', '.pagination-link', function() {
        var page = $(this).attr('id');
        
        load_data(page);
    })
});